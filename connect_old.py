#!/usr/bin/python

import PIL
from PIL import Image
import random
import struct
import socket, select
import errno
import time
from time import gmtime, strftime
from random import randint
from socket import error as socket_error

image = "mm.png"


#while True:
try:

    # open image
    myfile = open(image, 'rb')
    _bytes = myfile.read()
    size = struct.pack("!I", len(_bytes))


    s = socket.socket()
    host = '172.31.49.57'
    port = 5000

    print ("Trying to connect to host: " + host + ":" + str(port))
    s.connect((host, port))
    print ("Connected... Sending message to indicate image size")

    # send image size to server
    out1 = s.send(size)
    answer = s.recv(4096)

    print 'answer = %s' % answer
    print "sent " + str(out1) + "bytes"

    # send image to server
    if answer == 'SIZE OK':
        print "Sending image"
        out2 = s.send(_bytes)

        # check what server send
        answer = s.recv(4096)
        print 'answer = %s' % answer

        print " sent " + str(out2) + " bytes"

        if answer == 'GOT IMAGE' :
            s.sendall("BYE BYE")
            print 'Image successfully sent to server'

    myfile.close()

    #s.send(("Hello Aitor!").encode())
    print ("Image sent. Closing socket")

    #print "\n \n Info From Server: ", s.recv(1024)  #This gets printed after sometime
    s.close()
    time.sleep(10)


except socket_error as serr:
    if serr.errno == errno.ECONNREFUSED:
        print("Connection refused. Retrying in 10 seconds...")
        time.sleep(5)

        #else:
        #    print("Connection error. Quitting...")
        #    exit(0);
