#!/usr/bin/python

import PIL
from PIL import Image
import random
import struct
import socket, select
import errno
import time
import numpy
from time import gmtime, strftime
from random import randint
from socket import error as socket_error
import argparse


parser = argparse.ArgumentParser(description='Script to send some image data through a socket')
parser.add_argument('ip', help="private ip address")
parser.add_argument('port', help="server port")
parser.add_argument('file', type=argparse.FileType('r'), help="relative path to image")


def recv_timeout(the_socket,timeout=2):
    #make socket non blocking
    the_socket.setblocking(0)

    #total data partwise in an array
    total_data=[];
    data='';

    #beginning time
    begin=time.time()
    while 1:
        #if you got some data, then break after timeout
        if total_data and time.time()-begin > timeout:
            break

        #if you got no data at all, wait a little longer, twice the timeout
        elif time.time()-begin > timeout*2:
            break

        #recv something
        try:
            data = the_socket.recv(8192)
            if data:
                total_data.append(data)
                #change the beginning time for measurement
                begin=time.time()
            else:
                #sleep for sometime to indicate a gap
                time.sleep(0.1)
        except:
            pass

    #join all parts to make final string
    the_socket.setblocking(1)
    return ''.join(total_data)



# Parse the args
args = parser.parse_args()

img_path = args.file

# Load image as PIL Image
img = Image.open(img_path)
img_w, img_h = img.size

# Convert image to YUV
img_yuv = img.convert('L')

img_yuv = img_yuv.rotate(270)



# Image to numpy 2D array
#img_yuv_array = numpy.array(img_yuv)

# Get Y channel
#Y = img_yuv_array[:,:,0]

# Convert to bytes
Y_bytes = img_yuv.tobytes()

# Send through socket

try:
    # open image
    #myfile = open(image, 'rb')
    _bytes = Y_bytes
    size = struct.pack("!III", len(_bytes), img_w, img_h)


    s = socket.socket()
    host = args.ip # '172.31.49.57'
    port = int(args.port) # 5000

    print ("Trying to connect to host: " + host + ":" + str(port))
    s.connect((host, port))
    print ("Connected... Sending message to indicate image size. Message is: " + size)

    i = 0
    # send image size to server
    while i<3:
        out1 = s.send(size)
        print ("Sent message of size " + str(out1) + ". Waiting for answer...")
        answer = s.recv(4096)

        print 'Answer received. Answer = %s' % answer

        # send image to server
        if answer == 'SIZE OK':
            print "Sending image"
            out2 = s.send(_bytes)

            # check what server send
            answer = recv_timeout(s, 0.5)


            print 'answer = %s' % answer


            print " sent " + str(out2) + " bytes"





        print ("New image will be sent in 2")
        time.sleep(1)
        print ("New image will be sent in 1")
        i+=1




    s.close()


except socket_error as serr:
    if serr.errno == errno.ECONNREFUSED:
        print("Connection refused. Retrying in 10 seconds...")
        time.sleep(5)
